//
//  Animal.swift
//  group4_assignment5
//
//  Created by Geneivie Nguyen on 10/12/19.
//  Copyright © 2019 cs329e. All rights reserved.
//

import Foundation

class Animal {
    var name: String
    var scientificName: String
    var scientificClass: String
    var size: Double
    var image: String
    
    init (name: String, scientificName: String, scientificClass: String, size: Double, image: String) {
        self.name = name
        self.scientificName = scientificName
        self.scientificClass = scientificClass
        self.size = size
        self.image = image
    }
    
}
