//
//  AnimalsCollectionViewController.swift
//  group4_assignment5
//
//  Created by Geneivie Nguyen on 10/12/19.
//  Copyright © 2019 cs329e. All rights reserved.
//

import UIKit

class AnimalCollectionViewCell: UICollectionViewCell {
    
    //@IBOutlet weak var galleryImage: UIImageView!
    @IBOutlet weak var galleryCaption: UILabel!
    @IBOutlet weak var galleryImage: UIImageView!
    
}


class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let identifier = "CollectionCell"
    //var items = [String]()
    
    // create dictionary of instances
    var galleryItems = [GalleryItem]()
    var animalGallery: [AnimalGallery] = []
    var animalList = ["Rat", "Ox", "Tiger", "Rabbit", "Komodo Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"]

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accessPlist()
        
    }
    
    
    private func accessPlist() {
        // locate plist
        let inputFile = Bundle.main.path(forResource: "AnimalGallery", ofType: "plist")
        let inputDataArray = NSArray(contentsOfFile: inputFile!)
        
        // access plist and create new instance of GalleryItem
        for input in inputDataArray as! [Dictionary <String, String>] {
            
            for (key, value) in input {
                
                let galleryImage = UIImage(named: "\(key)")
                
                
                guard let newGalleryItem = GalleryItem(name: "\(key)", image: galleryImage, caption: "\(value)") else {
                    fatalError("Unable to instantiate animal")
                }

                galleryItems += [newGalleryItem]
                    
            }
            
            //create array of dictionaries for each animal
            animalGallery.append(AnimalGallery(gallery: galleryItems))
            
            //reinitializes galleryItems
            galleryItems = [GalleryItem]()
            
        }
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath as IndexPath) as! AnimalCollectionViewCell

        let animal = animalGallery[myIndex].gallery[indexPath.row]

        cell.galleryImage?.image = animal.image
        cell.galleryCaption?.text = animal.caption
        return cell
    }
    
    //MARK: UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = view.frame.size.width
        return CGSize(width: length, height: length)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //return UIEdgeInsets.init(top: 20, left: 10, bottom: 20, right: 10)
        return UIEdgeInsets.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GalleryCollectionReusableViewH", for: indexPath) as? GalleryCollectionReusableView
                else {
                    fatalError("Invalid view type")
            }
            
            let animalName = animalList[myIndex]
            headerView.headerLabel.text = animalName
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            guard
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GalleryCollectionReusableViewF", for: indexPath) as? GalleryCollectionReusableView
                else {
                    fatalError("Invalid view type")
            }
            
            footerView.footerLabel.text = "Animal Gallery by Group 4"
            return footerView
            
        default:
            assert(false, "Invalid element type")
    }
    }
    
    
}

