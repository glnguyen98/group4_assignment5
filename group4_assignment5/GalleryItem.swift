//
//  Gallery.swift
//  group4_assignment5
//
//  Created by Geneivie Nguyen on 10/12/19.
//  Copyright © 2019 cs329e. All rights reserved.
//

import UIKit


class GalleryItem {
    var name: String
    var image: UIImage?
    var caption: String
    
    init?(name: String, image: UIImage?, caption: String) {
        self.name = name
        self.image = image
        self.caption = caption
    }
}

struct AnimalGallery {
    let gallery: [GalleryItem]
}
