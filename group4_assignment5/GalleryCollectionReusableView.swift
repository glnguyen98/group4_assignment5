//
//  GalleryCollectionReusableView.swift
//  group4_assignment5
//
//  Created by Geneivie Nguyen on 10/14/19.
//  Copyright © 2019 cs329e. All rights reserved.
//

import UIKit

class GalleryCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var footerLabel: UILabel!
}
