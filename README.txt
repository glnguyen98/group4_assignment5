Multiple Galleries
Multiple galleries were created for individual animal cells. A different header is displayed for each animal.

Preserving Image Aspect Ratio
Top, bottom, and side constraints of the UIImage in the gallery were added to preserve the aspect ratio of the images. The content mode of the image was also set to Aspect Fill.